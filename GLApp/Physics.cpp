#include "Physics.h"

namespace Physics
{
	Shape::Shape(void)
	{
	}

	void Shape::setShapeType(Physics::Shape::ShapeType pShape)
	{
		mType = pShape;
	}

	Shape::~Shape(void)
	{

	}

	Body::Body(void)
	{
		mOwner = nullptr;
		mMaterial = nullptr;
		mMassData = nullptr;
	}

	void Body::setOwner(std::shared_ptr<SceneObject> pOwner)
	{
		mOwner = pOwner;
	}

	void Body::setMaterialData(Physics::Material* pMaterial)
	{
		mMaterial = pMaterial;
		//calculations need to be made here to calculate mass from volume & density

		//Density = volume * mass, so 
		//Mass = Density / volume
		//volume = shape width * shape height * shape depth for Cuboid
		// OR 
		//volume = shape height * PI*shape radius squared for cylinder
		//volume = 4/3*PI * radius cubed for sphere
		//Inverse Mass = 1.0/Mass

		//Inertia & inverse inertia must be handled too
	}

	void Body::setMassData(Physics::MassData* pMassData)
	{
		mMassData = pMassData;
	}

	void Body::setShape(Physics::Shape* pShape)
	{
		mShape = pShape;
	}

	Body::~Body(void)
	{
		delete mMassData;
		delete mShape;
		delete mMaterial;
	}

};