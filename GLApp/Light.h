#pragma once
#include <glm\glm.hpp>
struct LightData
{
    glm::vec4 mAmbient;
    glm::vec4 mDiffuse;
    glm::vec4 mSpecular;
    glm::vec4 mPosition;
    glm::vec4 mDirection;
	glm::vec3 mAttenuation;
    float mCutOffAngle;
    int mLightType; //0 for directional light, 1 for point light, 2 for spot
};

enum LightType
{
    DIRECTIONAL,
    POINT_LIGHT,
    SPOT_LIGHT
};

class Light
{
public:
    Light(void);
    void setAmbient(const glm::vec4& pAmbient);
    void setDiffuse(const glm::vec4& pDiffuse);
    void setSpecular(const glm::vec4& pSpecular);
    void setPosition(const glm::vec4& pPosition);
    void setDirection(const glm::vec4& pDirection);
    void setCutOffAngle(const float& pAngle);
    void setLightType(const LightType& pType);
	void setAttenuation(const glm::vec3& pAttenuation);
	void move(const glm::vec3& pOffset);
    LightData& getData();
    virtual ~Light(void);
private:
    LightData mData;
};

