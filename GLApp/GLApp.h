#pragma once
#include <memory>
#include <SDL.h>
#include <GL\glew.h>
#include <vector>
#include <glm\glm.hpp>

class Renderer;
class Timer;
class SceneManager;
class SceneObjectFactory;
class Controller;
class Camera;
class SceneObject;
class Light;

class GLApp
{

public:
    //ctor/dtor
    GLApp(void);
    ~GLApp(void);
    //public methods
    int init(int pWinWidth, int pWinHeight);
    void run();
    void exitFatalError(const char* pErrorMsg);

private:
    //internal operations
    void update(const float& pDeltaTimeS);
    void renderScene();
    bool handleEvent(SDL_Event sdlEvent);
    void shutDown();
    //internal variables
    bool mRunning;
    SDL_Window* mWindow;
    SDL_GLContext mContext;
    std::unique_ptr< Renderer > mRenderer;
    std::unique_ptr< Timer > mTimer;
    std::shared_ptr< Camera > mCamera;
    std::unique_ptr< SceneObjectFactory > mObjectFactory;
    std::unique_ptr< Controller > mController;
    std::vector< std::shared_ptr< SceneObject > > mObjects;
    std::shared_ptr<Light> mLight;
	int mWindowHeight,
		mWindowWidth;
    int mLightMod;
};

