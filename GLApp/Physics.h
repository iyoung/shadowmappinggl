#pragma once
#include <memory>
#include <glm\glm.hpp>

class SceneObject;

//TODO: Complete implementation for Body
//		Create Broad Phase class: Sweep and prune OR Quad/Oct tree
//		Create Pair Manager class: for temporal coherence
//		Create Narrow Phase class: SAT
//		Shape class/struct which stores data for cuboid, sphere, cylinder etc
//		Change data stored in Body to be smart pointers

namespace Physics
{
	//static physics calculation methods
	//calculate torque force (force applied to rotate stuff)
	//calculate normal force (force applied along collision normal, to correct object intersections)
	//calculate impulse (total force * time)
	//calculate friction force (perpendicular to the collision normal, and is used to create torque)
	//calculate air resistance  (slows stuff down, as it moves through the air)
	//calculate gravity force (gravity direction * gravity magnitude * body mass)

	//base shape class
	class Shape
	{
	public:
		enum ShapeType
		{
			CUBOID,		//cube type objects
			SPHERE,		//ball
			CYLINDER,	//tube
			PLANE,		//fancy word for rectangle
			POLYGON,	//fancy word for triangle
			CIRCLE,		//flat circular bounding area
		};
		Shape(void);
		virtual ~Shape(void);
		void setShapeType(Physics::Shape::ShapeType pShape);
		const float& getVolume();
		Physics::Shape::ShapeType getShapeType();
	protected:
		ShapeType mType;
		float mVolume; //in metres cubed
	};

	struct Material
	{
		float mDensity;
		float mRestitution;
		float mKin_Friction;
		float mStat_Friction;
	};

	struct MassData
	{
		float mMass;
		float mInv_Mass;
		float mInertia;
		float mInv_Inertia;
	};

	struct pair
	{
		Body* A;
		Body* B;
		//simple equality method to enable quick checking to see if the core engine has duplicate pairs
		bool operator ==(const pair& other) const
		{
			return ( ( (other.A == other.A) && (other.B == other.B) ) || ( (other.A == other.B) && (other.B == other.A) ) );
		}
		//ctor, dtor pair
		pair()
		{
			A = nullptr;
			B = nullptr;
		}
		~pair()
		{
			A = nullptr;
			B = nullptr;
		}
	};


	//-------------------------------------------------------//
	// Main Body class for storing physics related data		 //
	class Body
	{
	public:
		Body(void);
		//Method which takes in a material, and uses the volume of the transformed shape to calculate mass data
		void setMaterialData(Physics::Material* pMaterial);
		//Mass should not be calculated by hand, an empty mas data struct should be added for dynamic objects
		void setMassData(Physics::MassData* pMassData);
		//use this method to set the owning scene object. useful for getting the transform during updates
		void setOwner(std::shared_ptr<SceneObject> pOwner);
		void setShape(Physics::Shape* pShape);
		void setVelocity(glm::vec3& pVelocity);
		void setForce(glm::vec3& pForce);
		Physics::MassData* getMassData();
		Physics::Material* getMaterial();
		glm::vec3& getVelocity();
		glm::vec3& getForce();
		virtual ~Body(void);
	protected:
		std::shared_ptr<SceneObject> mOwner;
		Physics::Material* mMaterial;
		Physics::MassData* mMassData;
		Physics::Shape* mShape;
		glm::vec3 mVelocity;
		glm::vec3 mForce;
	};

};
