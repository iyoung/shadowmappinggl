#include "Renderer.h"
#include "Model.h"
#include <glm\gtc\matrix_transform.hpp>

Renderer::Renderer(void)
{
}
void Renderer::enableShadows(bool pSelect)
{
    mShadowsEnabled = pSelect;
}

int Renderer::init(const int& pWinWidth, const int& pWinHeight)
{

   
    if (openWindow(pWinWidth,pWinHeight) < 0)
    {
        printf("opengl init error\n");
        return -1;
    }
    printf( "Window: OK!\n" );

    mShadowBuffer = std::unique_ptr< FrameBuffer > ( new FrameBuffer() );

    if( mShadowBuffer->init(pWinWidth,pWinHeight) < 0)
    {
        printf("frame buffer error\n");
        return -1;
    }
    printf( "FBO: OK!\n" );

    if (loadShaders() < 0)
    {
        printf( "Shader error: see error log\n" );
        return -1;
    }
     printf( "Shaders: OK!\n" );
    return 0;
}

int Renderer::loadShaders()
{
    //create normal shader
    mLightShader = std::unique_ptr< Shader > ( new Shader() );
    if ( mLightShader->load( "shaders/lighting.vert", "shaders/lighting.frag" ) < 0)
    {
        printf("lighting shader error\n");
        return -1;
    }
    //create shadow shader
    mShadowShader = std::unique_ptr< Shader > ( new Shader() );
    if ( mShadowShader->load( "shaders/shadow.vert", "shaders/shadow.frag" ) < 0 )
    {
        printf("shadow shader error\n");
        return -1;
    }
	//create shadow shader
    mDepthShader = std::unique_ptr< Shader > ( new Shader );
    if ( mDepthShader->load( "shaders/depth.vert", "shaders/depth.frag" ) < 0 ) 
    {
        printf("depth shader error\n");
        return -1;
    }
    return 0;
}

int Renderer::openWindow(const int& pWinWidth, const int& pWinHeight)
{
	//initialize SDL
    int error = SDL_Init(SDL_INIT_VIDEO);
    if (error < 0) // Initialize video
    {
        return error;
    }
	//create a v3.3 opengl context
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)

    // Create window
    mWindow = SDL_CreateWindow("GLAPP", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        pWinWidth, pWinHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
    if(!mWindow)
    {
        return -1;
    }
    mContext = SDL_GL_CreateContext(mWindow); // Create opengl context and attach to window
	//enable double buffering
    SDL_GL_SetSwapInterval(1);
	//enable general settings
	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	//set clear colour for front & back buffers
    glClearColor(0.0,0.0,0.0,1.0);
	//this is needed for windows
#ifdef _WIN32
	glewExperimental = GL_TRUE;
#endif
	//initialize glew
	GLenum err = glewInit();
    if ( GLEW_OK != err ) 
    { // glewInit failed, something is seriously wrong
		return -1;
	}

    return 0;
}
void Renderer::addSceneObject(std::shared_ptr<SceneObject> pObject)
{
    mObjects.push_back( pObject );
}
void Renderer::addLight(std::shared_ptr<Light> pLight)
{
    mLight = pLight;
}
void Renderer::addCamera(std::shared_ptr<Camera> pCam)
{
    mCamera = pCam;
}
void Renderer::renderScene()
{
	glClear( GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT );
    if (mShadowsEnabled)
    {
        depthRender();
        mShadowShader->activate();
        glViewport(0,0,mWindowWidth,mWindowHeight);
        //generate view matrix
        glm::mat4 view(1.0f);
        mCamera->getViewMatrix(view,false);
	    //and projection matrix
        glm::mat4 projection(1.0f);
        mCamera->getProjectionMatrix(projection,true);
	    //bind shadow frame buffer for reading as texture
        mShadowBuffer->BindRead(GL_TEXTURE0);
	    //set light data for entire render cycle
        mShadowShader->setLight( mLight->getData() );
	    mShadowShader->setGLM_Vector( mLight->getData().mDirection, "lightDirection" );
	    //iterate through each object in scene
        for (size_t i = 0; i < mObjects.size(); i++)
        {
            //get model matrix and model data from scene object
            glm::mat4 model = mObjects[i]->getTransform();
            std::shared_ptr<Model> mesh = mObjects[i]->getModel();
		    mShadowShader->setGLM_Matrix( glm::mat3( glm::transpose( glm::inverse( view * model ) ) ), "normalMatrix" );
            //get view & projection matrix from camera
		    //set uniforms  
            mShadowShader->setGLM_Matrix( model, "Model" );
            mShadowShader->setGLM_Matrix( view, "View" );
            mShadowShader->setProjection( projection );
            mShadowShader->setGLM_Vector( mesh->getColour(), "meshColour" );
		    mShadowShader->setGLM_Matrix( shadowMatrix, "shadowMatrix" );
		    //Bind indexed mesh and draw it
            glBindVertexArray( mesh->getModelHandle() );
            if (mesh->isIndexed())
                glDrawElements( GL_TRIANGLES, mesh->getVertexCount(), GL_UNSIGNED_INT, 0 );
            else
                glDrawArrays(GL_TRIANGLES, 0, mesh->getVertexCount());
        }
	    //deactivate normal shader and swap buffers
        mShadowShader->deactivate();
    }
    else
    {
        mLightShader->activate();

        //generate view matrix
        glm::mat4 view(1.0f);
        mCamera->getViewMatrix(view,false);
	    //and projection matrix
        glm::mat4 projection(1.0f);
        mCamera->getProjectionMatrix(projection,true);

	    //set light data for entire render cycle
        mLightShader->setLight( mLight->getData() );
	    mLightShader->setGLM_Vector( mLight->getData().mDirection, "lightDirection" );
	    //iterate through each object in scene
        for (size_t i = 0; i < mObjects.size(); i++)
        {
            //get model matrix and model data from scene object
            glm::mat4 model = mObjects[i]->getTransform();
            std::shared_ptr<Model> mesh = mObjects[i]->getModel();
		    mLightShader->setGLM_Matrix( glm::mat3( glm::transpose( glm::inverse( view * model ) ) ), "normalMatrix" );
            //get view & projection matrix from camera
		    //set uniforms  
            mLightShader->setGLM_Matrix( model, "Model" );
            mLightShader->setGLM_Matrix( view, "View" );
            mLightShader->setProjection( projection );
            mLightShader->setGLM_Vector( mesh->getColour(), "meshColour" );
		   
		    //Bind indexed mesh and draw it
            glBindVertexArray( mesh->getModelHandle() );
            if (mesh->isIndexed())
                glDrawElements( GL_TRIANGLES, mesh->getVertexCount(), GL_UNSIGNED_INT, 0 );
            else
                glDrawArrays(GL_TRIANGLES, 0, mesh->getVertexCount());
        }
	    //deactivate normal shader and swap buffers
        mShadowShader->deactivate();
    }
    
    SDL_GL_SwapWindow(mWindow);   
}
void Renderer::depthRender()
{
	//bind shadow frame buffer for writing

	glm::mat4 biasMatrix(
		0.5f,0.0f,0.0f,0.0f,
		0.0f,0.5f,0.0f,0.0f,
		0.0f,0.0f,0.5f,0.0f,
		0.5f,0.5f,0.5f,1.0f);
	//glDisable(GL_CULL_FACE);
	//clear it
    //glCullFace( GL_FRONT );
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(2.0,2.0);
	glViewport( 0, 0, mWindowWidth, mWindowHeight );
    mShadowBuffer->BindWrite();
	glClearDepth(1.0);

    glClear( GL_DEPTH_BUFFER_BIT );
	//activate shadow buffer shader
    mDepthShader->activate();
    //get light data and generate view & projection matrix for the light
    LightData data = mLight->getData();
	glm::mat4 view(1.0f);
    glm::mat4 projection(1.0f);

    if (data.mLightType == DIRECTIONAL)
    {
        view = glm::lookAt( glm::vec3(data.mDirection), glm::vec3(0.0f), glm::vec3(0.0,1.0,0.0) );
        projection = glm::ortho(-10.0f,10.0f,-10.0f,10.0f,-10.0f,20.0f);
    }
    if (data.mLightType == SPOT_LIGHT || data.mLightType == POINT_LIGHT)
    {
        view = glm::lookAt( glm::vec3(data.mPosition), glm::vec3(data.mPosition)-glm::vec3(data.mDirection), glm::vec3(0.0,1.0,0.0) );
        glm::mat4 projection = glm::perspective<float>(data.mCutOffAngle*2,mWindowWidth/mWindowHeight,-1.0f,150.0f);
    }

	//calculate shadow matrix. This will be used later to transform from positions to shadow screen space for sampling
    glm::mat4 pv = projection * view;
	shadowMatrix = biasMatrix * pv;
	//for each object in scene
    for (size_t i = 0; i < mObjects.size(); i++)
    {
        //get model matrix and model data from scene object
        glm::mat4 model = mObjects[i]->getTransform();
        std::shared_ptr<Model> mesh = mObjects[i]->getModel();
        //set uniforms
        mDepthShader->setModelView( view * model );
        mDepthShader->setProjection( projection );
		//bind and draw mesh
        glBindVertexArray( mesh->getModelHandle() );

        if (mesh->isIndexed())
            glDrawElements( GL_TRIANGLES, mesh->getVertexCount(), GL_UNSIGNED_INT, 0 );
        else
            glDrawArrays( GL_TRIANGLES, 0, mesh->getVertexCount() );
    }
	//then deactivate shader and unbind shadow frame buffer
    mDepthShader->deactivate();
	mShadowBuffer->Unbind();
	glEnable(GL_CULL_FACE);
	glDisable(GL_POLYGON_OFFSET_FILL);
    glCullFace(GL_BACK);
}
void Renderer::shutDown()
{
    SDL_GL_DeleteContext(mContext);
    SDL_DestroyWindow(mWindow);
}
Renderer::~Renderer(void)
{
    mObjects.clear();
}
