#pragma once
#include <vector>
#include <memory>
class SceneObject;
class SceneObjectFactory
{
public:
    SceneObjectFactory(void);
    std::shared_ptr<SceneObject> createObject();
    void releaseObjects();
    virtual ~SceneObjectFactory(void);

protected:
    std::vector<std::shared_ptr<SceneObject> > mObjects;
};

