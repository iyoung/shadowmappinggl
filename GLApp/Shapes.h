#pragma once
#include "physics.h"
namespace Physics
{
	//sphere shape commonly used for explosions, lighting collision etc
	class Sphere:
		public Physics::Shape
	{
	public:
		Sphere(void);
		void setRadius(const float& pRadius);
		const float& getRadius();
		virtual ~Sphere(void);
	private:
		float mRadius;
	};

	//cuboid shape, very commonly used for OBB and AABB bounding boxes
	class Cuboid:
		public Physics::Shape
	{
	public:
		Cuboid(void);
		void setDimensions(const float& pWidth, const float& pHeight, const float& pDepth);
		virtual ~Cuboid(void);
	private:
		glm::vec3 mDimensions;

	};
};
