#include "Material.h"


Material::Material(void)
{
}
glm::vec4& Material::getAmbient()
{
	return mAmbient;
}
glm::vec4& Material::getDiffuse()
{
	return mDiffuse;
}
glm::vec4& Material::getSpecular()
{
	return mSpecular;
}
float& Material::getShininess()
{
	return mShininess;
}

Material::~Material(void)
{
}
