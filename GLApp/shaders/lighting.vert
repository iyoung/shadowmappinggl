#version 330
//vertex shader
in vec3 in_Positon;
in vec3 in_Normal;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;
uniform mat3 normalMatrix;
uniform vec4 lightDirection;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec4 direction;
	vec3 attenuation;
	float cutoff;
	int lightType;
};
uniform lightStruct light;
out VS_FS_Block
{
	vec3 transformedNormal;
	vec3 light_Dir;
	vec3 vVec;
	vec2 coord;
	vec3 LF_Dir;
} data;

void main (void)
{
	if(light.lightType == 0)
	{
		data.light_Dir = (View * light.direction).xyz;
	}
	if(light.lightType == 1 )
	{
		vec3 pos = (View * Model * vec4( in_Positon,1.0 ) ).xyz;
		vec3 pLight = (View * light.position ).xyz;
		data.vVec = -pos;

		data.light_Dir = pLight - pos;
	}
	if(light.lightType == 2)
	{
		vec3 pos = (View * Model * vec4( in_Positon,1.0 ) ).xyz;
		vec3 pLight = (View * light.position ).xyz;
		data.vVec = -pos;
		data.LF_Dir = pLight - pos;
		data.light_Dir = (View * light.direction).xyz;
	}
	data.transformedNormal = normalize( normalMatrix * in_Normal );
	gl_Position = Projection * View * Model* vec4(in_Positon,1.0);
}