#include "SceneObject.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm\gtx\euler_angles.hpp>
#include "Model.h"

SceneObject::SceneObject(void) : mID(0), mActive(false), mChanged(false), mModel(nullptr)
{
    mTransform = glm::mat4(1.0f);
    mPosition = glm::vec3(0.0f);
    mScale = glm::vec3(1.0f);

    mEulerAngles[PITCH] = 0.0f;
    mEulerAngles[YAW] = 0.0f;
    mEulerAngles[ROLL] = 0.0f;
}

void SceneObject::init()
{

}

void SceneObject::setModel(std::shared_ptr<Model> pModel)
{
    mModel = pModel;
}
void SceneObject::rotate(RotateAxis pAxis,  const float& pAngle)
{
    mEulerAngles[pAxis] += glm::radians(pAngle);
    mChanged = true;
}
void SceneObject::move(const glm::vec3& pOffset)
{
    mPosition+=pOffset;
    mChanged = true;
}

void SceneObject::setScale(const glm::vec3& pScale)
{
    mScale = pScale;
    mChanged = true;
}

std::shared_ptr<Model> SceneObject::getModel()
{
    return mModel;
}

void SceneObject::setPosition( const glm::vec3& pPosition )
{
    mPosition = pPosition;
    mChanged = true;
}

void SceneObject::update( const float& pDeltaTimeS )
{
    if( mActive )
    {
        if( mChanged )
        {
            mTransform = glm::mat4( 1.0f );
            //translate operations
            glm::mat4 translate(1.0f);
            mTransform = glm::translate( mTransform, mPosition );
            //rotate operations
            glm::mat4 rotation = glm::eulerAngleYXZ( mEulerAngles[ YAW ], mEulerAngles[ PITCH ], mEulerAngles[ ROLL ] );
            //scale operations
            glm::mat4 scale(1.0f); 
            scale = glm::scale( mTransform, mScale );
            mTransform = scale * rotation * translate;
            mChanged = false;
        }
    }
}

SceneObject::~SceneObject(void)
{

}
