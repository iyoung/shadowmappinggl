#include "FileIO.h"
#include <stdio.h>
#include <fstream>
#include <iostream>
#include <ostream>

namespace FILEIO
{
        char* FileIO::loadFile(const char *pFileName, int &pFileSize) 
        {
	        int size;
	        char * memblock;

	        // file read based on example in cplusplus.com tutorial
	        // ios::ate opens file at the end
	        std::ifstream file (pFileName, std::ios::in|std::ios::binary|std::ios::ate);

	        if (file.is_open()) {
		        size = (int) file.tellg(); // get location of file pointer i.e. file size
		        pFileSize = (int) size;
		        memblock = new char [size];
		        file.seekg (0, std::ios::beg);
		        file.read (memblock, size);
		        file.close();
		        std::cout << "file " << pFileName << " loaded" << std::endl;
	        }
	        else {
		        std::cout << "Unable to open file " << pFileName << std::endl;
		        pFileSize = 0;
		        // should ideally set a flag or use exception handling
		        // so that calling function can decide what to do now
		        return nullptr;
	        }
	        return memblock;
        }

        std::vector<std::string>& loadFile(const std::string& pFileName)
        {
            std::ifstream inFile( pFileName, std::ios_base::in );
            std::vector<std::string> data;

            if(!inFile.is_open())
	        {
		        std::cout << "file not opened" << std::endl;
		        return data;
	        }

            while(!inFile.eof())
            {
                char buff[255];
                inFile.getline(buff,255);
                data.push_back( std::string(buff) );
            }

            return data;
        }

        void FileIO::saveFile(const char* pDestFile, const char* pData)
        {
            std::ofstream file;
            file.open(std::string(pDestFile));
            file << pData;
            file.close();
        }
};