#pragma once
#include <glm\glm.hpp>
#include <memory>

class Model;
enum RotateAxis
{
    PITCH,
    YAW,
    ROLL
};

class SceneObject
{
public:

    SceneObject(void);

    void init();
    void update(const float& pDeltaTimeS);
    void deactivate(){ mActive = false;}
    void activate(){ mActive = true;}

    void setPosition(const glm::vec3& pPosition);
    void move(const glm::vec3& pOffset);
    void rotate(RotateAxis pAxis, const float& pAngle);
    const glm::vec3& getPosition() { return mPosition; }
    const glm::mat4 getTransform() { return mTransform;}
    unsigned int getID(){ return mID; }
    void setID( const unsigned int& pID ){ mID = pID;}
    bool isActive(){return mActive;}
    void setScale(const glm::vec3& pScale);
    void setModel(std::shared_ptr<Model> pModel);
    std::shared_ptr<Model> getModel();
    ~SceneObject(void);

private:

    bool mChanged;
    bool mActive;
    unsigned int mID;
    float mEulerAngles[3];
    glm::mat4 mTransform;
    glm::vec3 mPosition;
    glm::vec3 mScale;
    glm::vec3 mOrientation;
    std::shared_ptr<Model> mModel;
};

