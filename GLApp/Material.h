#pragma once
#include <glm\glm.hpp>

class Material
{
public:
	Material(void);
	void setAmbient(const glm::vec4& pAmbient){mAmbient = pAmbient;}
	void setDiffuse(const glm::vec4& pDiffuse){mDiffuse = pDiffuse;}
	void setSpecular(const glm::vec4& pSpecular){mSpecular = pSpecular;}
	void setShininess(const float& pShininess){mShininess = pShininess;}
	glm::vec4& getAmbient();
	glm::vec4& getDiffuse();
	glm::vec4& getSpecular();
	float& getShininess();
	~Material(void);
private:
	glm::vec4 mAmbient;
	glm::vec4 mDiffuse;
	glm::vec4 mSpecular;
	float mShininess;
};

