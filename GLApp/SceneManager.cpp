#include "SceneManager.h"
#include "SceneObject.h"

SceneManager::SceneManager(void)
{
}
void SceneManager::addObject(std::shared_ptr<SceneObject> pObject)
{
    mObjects.push_back(pObject);
}

void SceneManager::removeObject(const unsigned int& pObjID)
{
    auto iter = mObjects.begin();

    while( iter != mObjects.end() )
    {
        if( (*iter)->getID() != pObjID )
        {
            iter++;
            continue;
        }

        mObjects.erase(iter);
        break;
    }
}

void SceneManager::update(const float& pDeltaTimeS)
{
    //call update functions on sceneobjects
    auto iter = mObjects.begin();

    while( iter != mObjects.end() )
    {
        if( (*iter)->isActive() )
            (*iter)->update(pDeltaTimeS);

        iter++;
    }
}

SceneManager::~SceneManager(void)
{
    mObjects.clear();
}
