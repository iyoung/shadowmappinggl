#pragma once
#include <vector>
#include <string>

namespace FILEIO
{
    class FileIO
    {
    public:
        static char* loadFile(const char *pFileName, int &pFileSize);
        static std::vector<std::string>& loadFile(const std::string& pFileName);
        static void saveFile(const char* pDestFile, const char* pData);
        //TODO: complete this method
        static void constructFilePath(char* pDestPath, char* pSourcePath){;}
    };
};
