#include "Shapes.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace Physics
{
	Sphere::Sphere(void)
	{
		setShapeType(SPHERE);
	}
	void Sphere::setRadius(const float& pRadius)
	{
		mVolume = (4/3) * pRadius * (M_PI * M_PI * M_PI);
		mRadius = pRadius;
	}

	Sphere::~Sphere(void)
	{

	}
	Cuboid::Cuboid(void)
	{
		
	}
	void Cuboid::setDimensions(const float& pWidth, const float& pHeight, const float& pDepth)
	{
		mVolume = pWidth * pHeight * pDepth;
		mDimensions.y = pHeight;
		mDimensions.x = pWidth;
		mDimensions.z = pDepth;
	}

	Cuboid::~Cuboid(void)
	{

	}
};