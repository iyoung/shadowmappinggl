#include "FrameBuffer.h"
#include <stdio.h>

FrameBuffer::FrameBuffer(void)
{
    m_fbo = 0;
    m_shadowMap = 0;
}
int FrameBuffer::init(int pWidth, int pHeight)
{
    glGenFramebuffers(1, &m_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

	// Depth texture. Slower than a depth buffer, but you can sample it later in your shader

    glGenTextures(1, &m_shadowMap);
	glBindTexture(GL_TEXTURE_2D, m_shadowMap);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT16, pWidth, pHeight, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		 
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, m_shadowMap, 0);

	// No color output in the bound framebuffer, only depth.
	glDrawBuffer(GL_NONE);

	// Always check that our framebuffer is ok
    GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (Status != GL_FRAMEBUFFER_COMPLETE) 
    {
        printf("FB error, status: 0x%x\n", Status);
        return -1;
    }
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return 0;
}
void FrameBuffer::BindWrite()
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fbo);
}
void FrameBuffer::BindRead(GLenum pTextureUnit)
{
    glActiveTexture(pTextureUnit);
    glBindTexture(GL_TEXTURE_2D, m_shadowMap);
}
void FrameBuffer::Unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
FrameBuffer::~FrameBuffer(void)
{
	glDeleteFramebuffers(1,&m_fbo);
	glDeleteTextures(1,&m_shadowMap);
}
