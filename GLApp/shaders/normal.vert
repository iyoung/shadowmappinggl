#version 330
//vertex shader
in vec3 in_Positon;

uniform mat4 ModelView;
uniform mat4 Projection;

void main (void)
{
	gl_Position = Projection * ModelView* vec4(in_Positon,1.0);
}