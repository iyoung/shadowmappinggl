#pragma once
#include <vector>
#include <memory>
class SceneObject;

class SceneManager
{
public:
    SceneManager(void);
    void update(const float& pDeltaTimeS);
    void addObject(std::shared_ptr<SceneObject> pObject);
    void removeObject(const unsigned int& pObjID);
    ~SceneManager(void);
private:
    std::vector<std::shared_ptr<SceneObject>> mObjects;
};

