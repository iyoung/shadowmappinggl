#include "GLApp.h"
#include <iostream>

int main(int argc, char **argv)
{
    //create an instance of the GLApp class
    std::unique_ptr< GLApp > app = std::unique_ptr< GLApp >( new GLApp() );
    //initialize and run the app
    int error = app->init( 800, 600 );
    if ( error < 0 )
    {
        std::cin.get();
        return error;
    }
    app->run();
    return error;
}
